"use strict";


/**************************************************************************************/

/**
 * Родительский класс для инструментов рисования
 * @param state
 * @param settings
 * @constructor
 */
function Figure(state, settings) {

    if (settings === undefined) {
        settings = {
            color: "#000000",
            width: "2",
        };
    }

    if (state === undefined) {
        state = 'ready';
    }


    this.state = state;
    this.settings = settings;
    this.$canvasDom = false;
    this.$input = false;
}

/**
 * метод изменения настроек фигуры
 * @param paramName
 * @param value
 */
Figure.prototype.changeSettings = function (paramName, value) {
    this.settings[paramName] = value;
};
/**
 * метод, рисующий фигуру на  canvas
 * @param $canvas
 */
Figure.prototype.show = function ($canvas) {
};

/**
 * принимае объект canvas. На нем в последсвии будут нарисованы фигуры
 * @param $canvasDom
 */
Figure.prototype.setLayer = function ($canvasDom) {
    this.$canvasDom = $canvasDom;
};


/**
 * принимает объект input. в него записываются параметры фигуры
 * @param $input
 */
Figure.prototype.setInput = function ($input) {
    this.$input = $input;
};


Figure.prototype.importFigure = function () {
    this.$input.val(JSON.stringify({
        figureName: this.constructor.name,
        settings: this.settings,
    }));
};


/**
 * возвращает контекс для рисования по canvas
 * @returns {*|CanvasRenderingContext2D|CanvasRenderingContext2D|WebGLRenderingContext}
 */
Figure.prototype.getContext = function () {
    return this.$canvasDom[0].getContext('2d');
};

/**
 * Возвращает список событий, необходимых для рисования фигуры. в классе наследнике должны быть реализованы одноименные методы
 * @returns {Array}
 */
Figure.prototype.getEventList = function () {
    return [];

};

/**
 * очистка canvas от нарисованного
 */
Figure.prototype.reset = function () {

    this.getContext().clearRect(0, 0, this.$canvasDom[0].width, this.$canvasDom[0].height);


};


/**************************************************************************************/

/**
 * Инструмент рисования "Линия"
 * @constructor
 */
function LineFigure() {
    Figure.apply(this, arguments);
}

LineFigure.prototype = Object.create(Figure.prototype);
LineFigure.prototype.constructor = LineFigure;
LineFigure.prototype.show = function () {

    if (this.settings.start && this.settings.end) {
        let ctx = this.getContext();
        ctx.beginPath();

        if (this.settings.color) {
            ctx.strokeStyle = this.settings.color;
        }
        if (this.settings.width) {
            ctx.lineWidth = this.settings.width;
        }
        ctx.moveTo(this.settings.start.x, this.settings.start.y);
        ctx.lineTo(this.settings.end.x, this.settings.end.y);
        ctx.stroke();
    }

};


LineFigure.prototype.getEventList = function () {
    return ['mousedown', 'mousemove'];

};


LineFigure.prototype.mousedown = function (x, y) {

    switch (this.state) {
        case'new':
            this.settings.start = {x: x, y: y};
            this.state = 'first';
            break;
        case 'first':
            this.reset();
            this.settings.end = {x: x, y: y};
            this.state = 'ready';
            break;
    }

    this.show();

    return this.state;
};


LineFigure.prototype.mousemove = function (x, y) {

    switch (this.state) {
        case 'first':
            this.settings.end = {x: x, y: y};

            this.reset();
            this.show();
            break;
    }

    return this.state;
};


/**************************************************************************************/

/**
 * Инструмент рисования "Прямоугольник"
 * @constructor
 */

function RectangleFigure() {
    LineFigure.apply(this, arguments);
}

RectangleFigure.prototype = Object.create(LineFigure.prototype);
RectangleFigure.prototype.constructor = RectangleFigure;
RectangleFigure.prototype.show = function () {

    if (this.settings.start && this.settings.end) {


        let ctx = this.getContext();


        ctx.beginPath();

        if (this.settings.color) {
            ctx.strokeStyle = this.settings.color;
        }
        if (this.settings.width) {
            ctx.lineWidth = this.settings.width;
        }

        ctx.rect(this.settings.start.x, this.settings.start.y, this.settings.end.x - this.settings.start.x, this.settings.end.y - this.settings.start.y);
        ctx.stroke();
    }

};


/**
 * Инструмент рисования "Круг"
 * @constructor
 */

function CircleFigure() {
    LineFigure.apply(this, arguments);
}

CircleFigure.prototype = Object.create(LineFigure.prototype);
CircleFigure.prototype.constructor = CircleFigure;
CircleFigure.prototype.show = function () {

    if (this.settings.start && this.settings.end) {


        let ctx = this.getContext();

        ctx.beginPath();


        const radius = Math.pow(Math.pow(this.settings.end.x - this.settings.start.x, 2) + Math.pow(this.settings.end.y - this.settings.start.y, 2), 0.5);
        ctx.arc(this.settings.start.x, this.settings.start.y, radius, 0, 2 * Math.PI, false);


        if (this.settings.color) {
            ctx.strokeStyle = this.settings.color;
        }
        if (this.settings.width) {
            ctx.lineWidth = this.settings.width;
        }

        ctx.stroke();
    }

};


/**************************************************************************************/

/**
 * Инструмент рисования "Карандаш"
 * @constructor
 */
function PenFigure() {
    LineFigure.apply(this, arguments);
}

PenFigure.prototype = Object.create(LineFigure.prototype);
PenFigure.prototype.constructor = PenFigure;
PenFigure.prototype.show = function () {

    if (typeof this.settings.points !== 'undefined') {


        let ctx = this.getContext();

        ctx.beginPath();


        ctx.moveTo(this.settings.points[0].x, this.settings.points.y);

        for (let i = 1; i < this.settings.points.length; i++) {

            ctx.lineTo(this.settings.points[i].x, this.settings.points[i].y);
        }

        if (this.settings.color) {
            ctx.strokeStyle = this.settings.color;
        }
        if (this.settings.width) {
            ctx.lineWidth = this.settings.width;
        }

        ctx.stroke();
    }

};


PenFigure.prototype.getEventList = function () {

    return ['mousedown', 'mousemove', 'mouseup'];

};

PenFigure.prototype.mousedown = function (x, y) {

    this.settings.points = [];
    this.settings.points.push({x: x, y: y});
    this.state = 'first';
    this.show();
};

PenFigure.prototype.mousemove = function (x, y) {

    if (this.state === 'first') {
        let lastPosition = this.settings.points.length - 1;
        if (lastPosition >= 0 &&
            (
                Math.abs(this.settings.points[lastPosition].x - x) > 5 ||
                Math.abs(this.settings.points[lastPosition].y - y) > 5
            )) {
            this.settings.points.push({x: Math.round(x), y: Math.round(y)});
            this.reset();
            this.show();
        }
    }
    return this.state;
};

PenFigure.prototype.mouseup = function (x, y) {
    if (typeof this.settings.points !== "undefined") {
        this.settings.points.push({x: x, y: y});
        this.reset();
        this.show();
    }
    this.state = 'ready';


    /**************************************************************************************/

    /**
     * Инструмент рисования "Карандаш"
     * @constructor
     */
    return this.state;
};


/**************************************************************************************/

/**
 * класс работает со слоями canvas  сохраняет элементы
 * @param $canvasBlock
 * @param $inputBlock
 * @constructor
 */
function Canvas($canvasBlock, $inputBlock) {

//определение координат мыши
    this.getMousePos = function (canvas, evt) {
        let rect = canvas.getBoundingClientRect(), // abs. size of element
            scaleX = canvas.width / rect.width,    // relationship bitmap vs. element for X
            scaleY = canvas.height / rect.height;  // relationship bitmap vs. element for Y

        return {
            x: (evt.clientX - rect.left) * scaleX,   // scale mouse coordinates after they have
            y: (evt.clientY - rect.top) * scaleY     // been adjusted to be relative to element
        };
    };


    let self = this;

    this.canvasCount = 0;
    this.$tempCanvasLayer = false;
    this.$tempFigure = false;
    this.$inputBlock = $inputBlock;
    this.$canvasBlock = $canvasBlock;


    this.objects = [];


    this.callEvent = function (event) {

        if (self.$tempFigure instanceof Figure) {
            let cords = self.getMousePos(this, event);
            let eventType = event.type;
            if (typeof self.$tempFigure[eventType] === 'function') {
                const figureState = self.$tempFigure[eventType](cords.x,
                    cords.y
                );

                if (figureState === 'ready') {
                    self.saveNewFigure(self.$tempFigure);
                    self.addLayer();
                    self.start();
                }
            } else {
                return false;
            }
        }
        return true;
    };


    this.getNewInputObject = function () {
        return $("<input type='hidden' name='CANVAS[UF_DATA][]' value=''>");
    };

    this.getNewCanvasObject = function () {
        return $('<canvas width="400" height="400"   style=" position: absolute; left: 0; top: 0; z-index: ' + this.canvasCount++ + '" >');
    };

    this.import = function () {
// помещаем объекты в хранилище

        $('input', this.$inputBlock).each(function (index, element) {
            try {
                let ar = JSON.parse($(element).val());

                let figureName = ar['figureName'];
                self.objects.push(new window[figureName]('ready', ar['settings']));
            } catch (e) {
                console.log(e.getMessage());
            }
        });
    };


    this.show = function () {
//объекты из хранилища
        for (let i = 0; i < this.objects.length; i++) {
            if (this.objects[i] instanceof Figure) {
                let $canvasDom = this.getNewCanvasObject();
                this.$canvasBlock.append($canvasDom);
                this.objects[i].setLayer($canvasDom);
                this.objects[i].show();
            }
        }


    };
    /**
     *
     * @param  figure {Figure}
     * @returns {boolean}
     */
    this.saveNewFigure = function (figure) {
        this.objects.push(figure);
        let $input = this.getNewInputObject();

        figure.setInput($input);
        this.$inputBlock.append($input);
        figure.importFigure();
        this.$tempCanvasLayer.off();
        this.$tempFigure = false;
        return true;
    };


    this.changeColorLine = function (color) {
        if (!( this.$tempFigure instanceof Figure)) {
            return false;
        }

        this.$tempFigure.changeSettings('color', color);

        this.$tempFigure.reset();
        this.$tempFigure.show();
        return true;

    };
    this.changeWidthLine = function (width) {
        if (!(this.$tempFigure instanceof Figure)) {
            return false;
        }
        this.$tempFigure.changeSettings('width', width);
        this.$tempFigure.reset();
        this.$tempFigure.show();
        return true;
    };

    this.changeFigure = function (figureName, color, width) {
        if (this.$tempFigure instanceof Figure) {
            this.$tempFigure.reset();
            this.$tempCanvasLayer.off();
        }
        let settings = {
            color: color,
            width: width
        };

        //  this.$tempFigure=  new window[figureName]('ready', settings);
        //
        switch (figureName) {
            case "LineFigure":
                this.$tempFigure = new LineFigure('new', settings);
                break;
            case "CircleFigure":
                this.$tempFigure = new CircleFigure('new', settings);
                break;
            case "RectangleFigure":
                this.$tempFigure = new RectangleFigure('new', settings);
                break;
            case "PenFigure":
                this.$tempFigure = new PenFigure('new', settings);
                break;
            default:
                return false;
        }

        this.$tempFigure.setLayer(this.$tempCanvasLayer);


        this.addEvents();
        return true;
    };


    this.addLayer = function () {
        this.$tempCanvasLayer = this.getNewCanvasObject();
        this.$canvasBlock.append(this.$tempCanvasLayer);

    };


    this.addEvents = function () {


        this.$tempFigure.setLayer(this.$tempCanvasLayer);

        let arEventList = this.$tempFigure.getEventList();
        for (let i = 0; i < arEventList.length; i++) {

            this.$tempCanvasLayer.on(arEventList[i], this.callEvent);
        }


    };

    this.start = function () {
        if (typeof currentParams !== "undefined") {
            this.changeFigure(currentParams.currentFigureName, currentParams.currentColor, currentParams.currentWidth);
        }
    };
}


/**
 * Класс для работы с панелью рисования
 * @constructor
 */
function CurrentParams() {
    this.currentWidth = 2;
    this.currentColorDec = 8;
    this.currentColor = '#000000';
    this.currentFigureName = "LineFigure";
}


CurrentParams.prototype.checkCurrentValues = function () {
    this.currentWidth = $('input[name=widthFigure]').val();
    this.currentColorDec = $('input[name=colorFigure]').val();
    this.currentFigureName = $('input[name=figureName]:checked').val();
    ;
    this.currentColor = this.convertDecToHexColor(this.currentColorDec);
};


CurrentParams.prototype.convertDecToHexColor = function (colorDec) {
    colorDec = 1 * colorDec;
    let colorHex = colorDec.toString(16);
    return '#' + colorHex + colorHex + colorHex;
};


CurrentParams.prototype.setWight = function (wight) {
    this.currentWidth = wight;
    if (typeof canvas !== "undefined") {
        canvas.changeWidthLine(wight);
    }
};

CurrentParams.prototype.setColor = function (colorDec) {
    this.currentColor = this.convertDecToHexColor(colorDec);
    if (typeof canvas !== "undefined") {
        canvas.changeColorLine(this.currentColor);
    }
};

CurrentParams.prototype.setFigureName = function (figureName) {

    this.currentFigureName = figureName;
    if (typeof canvas !== "undefined") {
        canvas.changeFigure(figureName, this.currentColor, this.currentWidth);
    }
};


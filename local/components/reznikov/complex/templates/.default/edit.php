<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
$APPLICATION->IncludeComponent("reznikov:script", '', [], $component);
$APPLICATION->IncludeComponent("reznikov:edit", "", [
    'HLBLOCK_ID' =>$arParams['HLBLOCK_ID'],"ID"=>$arResult["VARIABLES"]["ID"],
    "LIST_URL"=>$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["list"],
    "EDIT_URL"=>$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["edit"]
],$component
);
?>

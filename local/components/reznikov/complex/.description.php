<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Комплексный компонент редактируемых картинок",
	"DESCRIPTION" => "не кешируется",
	"ICON" => "/images/include.gif",
    "COMPLEX" => "Y",
	"PATH" => array(
		"ID" => "utility",
		"CHILD" => array(
			"ID" => "complex",
			"NAME" => "Сервис",
		),
	),
);
?>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */
/** @global CUserTypeManager $USER_FIELD_MANAGER */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Iblock;
use Bitrix\Currency;

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(

        "HLBLOCK_ID" => Array(
            "NAME" => "ID блока"
        ),

        "SEF_MODE" => Array(
            "list" => array(
                "NAME" => 'страница списка',
                "DEFAULT" => "",
                "VARIABLES" => array(),
            ),
            "edit" => array(
                "NAME" => 'страница редактирования',
                "DEFAULT" => "#ID#/",
                "VARIABLES" => array("SECTION_ID"),
            )

        )
    ));
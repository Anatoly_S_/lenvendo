<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

?>
<div class="container">
    <div class="row">
        <?
        foreach ($arResult["ITEMS"] as $arItem) {
            ?>
            <div class="col-xs-6" style="height: 500px; width: 400px;margin: 10px">

                <div id="canvas-block<?= $arItem["ID"] ?>"
                     style=" border: 2px solid red; width: 404px; height: 404px;position: relative; padding:2px"></div>
                <div id="canvas-form<?= $arItem["ID"] ?>">
                    <?php foreach (unserialize($arItem["UF_DATA"]) as $strData):
                        ?>
                        <input type='hidden' name="CANVAS[UF_DATA][]" value="<?php echo htmlspecialcharsbx($strData,ENT_QUOTES) ?>"/>
                        <?
                    endforeach; ?>
                </div>
                <a href="<?= str_replace('#ID#',$arItem["ID"],$arParams['EDIT_URL']) ?>">редактировать</a>

                <script>$(document).ready(function () {
                        let canvas = new Canvas($("#canvas-block<?= $arItem["ID"] ?>"), $("#canvas-form<?= $arItem["ID"] ?>"));

                        canvas.import();
                        canvas.show();
                    });
                </script>
            </div>

            <?


        }
        ?>
        <div class="col-xs-6" style="height: 500px; width: 400px  ; margin : 10px">


            <a href="0/">Создать новый рисунок</a>

        </div>

    </div>
</div><?

$APPLICATION->IncludeComponent(
    'bitrix:main.pagenavigation',
    '',
    array(
        'NAV_OBJECT' => $arResult['nav_object'],
        'SEF_MODE' => 'N',
    ),
    false
);
?>

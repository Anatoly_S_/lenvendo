<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */
/** @global CUserTypeManager $USER_FIELD_MANAGER */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Iblock;
use Bitrix\Currency;

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
                "HLBLOCK_ID" => Array("NAME"=>"ID блока"
        ),
    )
);
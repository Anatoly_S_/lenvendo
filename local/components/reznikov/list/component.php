<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();


use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$hlClass = false;
if (\Bitrix\Main\Loader::includeModule('highloadblock')) {
    $hlblock = HL\HighloadBlockTable::getList(Array('filter' => Array("ID" => $arParams["HLBLOCK_ID"])))->fetch();

    $entity = false;
    if ($hlblock) {
        $entity = HL\HighloadBlockTable::compileEntity($hlblock); //генерация класса
    }
    if (is_object($entity)) {
        $hlClass = $entity->getDataClass();
    }
}

if ($hlClass === false) {
    throw new Error("Неверные настройки компонента. HL блок отсутсвует или модуль не подключен4");

}



$fields = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('HLBLOCK_'.$arParams["HLBLOCK_ID"], 0, LANGUAGE_ID);

// sort
$sortBy = 'ID';
$sortOrder = 'DESC';

// pagen
    $pagenId = 'page';
    $perPage = 4;
    $nav = new \Bitrix\Main\UI\PageNavigation($pagenId);
    $nav->allowAllRecords(true)
        ->setPageSize($perPage)
        ->initFromUri();



// start query
$mainQuery = new Entity\Query($hlClass::getEntity());
$mainQuery->setSelect(array('*'));
$mainQuery->setOrder(array($sortBy => $sortOrder));


// pagen
if ($perPage > 0)
{
    $mainQueryCnt = $mainQuery;
    $result = $mainQueryCnt->exec();
    $result = new CDBResult($result);
    $nav->setRecordCount($result->selectedRowsCount());
    $arResult['nav_object'] = $nav;
    unset($mainQueryCnt, $result);

    $mainQuery->setLimit($nav->getLimit());
    $mainQuery->setOffset($nav->getOffset());
}

// execute query
$result = $mainQuery->exec();
$result = new CDBResult($result);

// build results
$arRows = array();
$tableColumns = array();

while ($row = $result->fetch())
{
    $arResult['ITEMS'][] = $row;
}




$this->IncludeComponentTemplate();
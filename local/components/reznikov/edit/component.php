<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
use Bitrix\Main;
use Bitrix\Main\Context;
use Bitrix\Highloadblock as HL;

$templateName = "";


try {

    $arResult["CANVAS_ITEMS"] = [];
    $arResult["SHOW_PASS_FORM"] = "N";

    if ($arParams["AJAX_MODE"] == "Y") {
        $ajaxSession = CAjax::GetSession();
        if ($ajaxSession && $arParams["AJAX_ID"] != $ajaxSession) {
            return;
        }
    }
    /**
     * @var \Bitrix\Main\Entity\DataManager|boolean $hlClass
     */
    $hlClass = false;
    if (\Bitrix\Main\Loader::includeModule('highloadblock')) {
        $hlblock = HL\HighloadBlockTable::getList(Array('filter' => Array("ID" => $arParams["HLBLOCK_ID"])))->fetch();

        $entity = false;
        if ($hlblock) {
            $entity = HL\HighloadBlockTable::compileEntity($hlblock); //генерация класса
        }
        if (is_object($entity)) {
            $hlClass = $entity->getDataClass();
        }
    }

    if ($hlClass === false) {
        throw new Exception("Неверные настройки компонента. HL блок отсутсвует или модуль не подключен");

    }

    $arResult["ID"] = intval($arParams["ID"]);

    $arResult["ITEM"] = [
        "ID" => "0",
        "UF_PASSWORD" => "",
        "UF_DATA" => serialize([]),
    ];
    $arResult["UF_PASSWORD"] = "";


    if ($arResult["ID"]) {
        $arResult["ITEM"] = $hlClass::getById($arResult["ID"])->fetch();
        if ($arResult["ITEM"] == false) {
            define("ERROR_404", "Y");
            \CHTTP::setStatus("404 Not Found");
            throw new Exception('Рисунок не найден');
        }

        $arResult["SHOW_PASS_FORM"] = isset($_SESSION["CANVAS"]["CHECK_UF_PASSWORD"][$arResult["ID"]]) ? "N" : "Y";

        $arResult["UF_PASSWORD"] = $arResult["ITEM"]["UF_PASSWORD"];
    }

    if ($arResult["SHOW_PASS_FORM"] == "Y") {
        $templateName = "password";
    }
    $arResult["CANVAS_ITEMS"] = unserialize($arResult["ITEM"]["UF_DATA"]);


    if (!is_array($arResult["CANVAS_ITEMS"])) {
        throw new Exception('ошибка чтения рисунка');
    }

    $request = Context::getCurrent()->getRequest();
    if ($request->isPost() && check_bitrix_sessid() && is_array($request->get("CANVAS"))) {
        $arFields = $request->get("CANVAS");

        if ($arFields["ACTION"] == 'password') {
            if ($arFields["UF_PASSWORD"] == $arResult["ITEM"]["UF_PASSWORD"]) {
                $_SESSION["CANVAS"]["CHECK_UF_PASSWORD"][$arResult["ID"]] = "Y";
                $templateName = '';
            } else {
                $arResult["ERROR"][] = "Не верный пароль";
            }
        } elseif ($arFields["ACTION"] == 'save') {

            if (!is_array($arFields) || $arFields["ID"] != $arResult["ID"] || $arResult["SHOW_PASS_FORM"] == "Y") {
                $arResult["ERROR"][] = "Нет доступа";
            } else
                if (!is_array($arFields["UF_DATA"]) && !empty($arFields["UF_DATA"])) {
                    $arResult["ERROR"][] = "Рисунок не обнаружен";
                }

            $arResult["CANVAS_ITEMS"] = $arFields["UF_DATA"] ?? [];


            if (empty($arResult["ERROR"])) {
                $arData = [
                    "UF_DATA" => serialize($arFields["UF_DATA"])
                ];

                if ($arResult["ID"]) {
                    $result = $hlClass::update($arResult["ID"], $arData);
                } else {
                    $arResult["UF_PASSWORD"] = randString();

                    $arData["UF_PASSWORD"] = $arResult["UF_PASSWORD"];

                    $result = $hlClass::add($arData);
                }
                if (!$result->isSuccess()) {
                    $arResult["ERROR"][] = $result->getErrorMessages();

                } else {
                    if ($result instanceof Main\Entity\AddResult) {
                        $arResult["ID"] = $result->getId();
                        $_SESSION["CANVAS"]["CHECK_UF_PASSWORD"][$arResult["ID"]] = "Y";
                    }

                    $templateName = 'success';
                }
            }
        }

    }

} catch (Exception $e) {
    $templateName = 'error';

    $arResult["ERROR"][] = $e->getMessage();
}


$this->IncludeComponentTemplate($templateName);


?>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.2/bootstrap-slider.min.js');
Asset::getInstance()->addCss('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.2/css/bootstrap-slider.min.css');


Asset::getInstance()->addJs($templateFolder . "/figure.js");
Asset::getInstance()->addJs($templateFolder . "/linefigure.js");
Asset::getInstance()->addJs($templateFolder . "/canvas.js");


?>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div>
                <h2>Толщина линии</h2>
                <form>
                    Тоньше <input id="ex5" type="text" name="widthFigure" data-slider-min="2" data-slider-max="10"
                                  data-slider-step="1"
                                  data-slider-value="3"/> Толще <br>
                    <div>Сейчас: <span style="display: inline-block;width: 50px; height: 3px;background-color: black;"
                                       id="ex5SliderVal"></span></div>
                </form>
            </div>


            <div>
                <h2>Цвет</h2>
                <form>
                    Темнее <input id="ex6" type="text" name="colorFigure" data-slider-min="0" data-slider-max="200"
                                  data-slider-step="1"
                                  data-slider-value="100"/> Светлее <br>
                    <div style="height:60px; line-height: 30px;">

                        <span>Сейчас:</span>

                        <span style="display: inline-block;width: 50px; height: 50px;background-color: rgb(100,100,100)"
                              id="ex6SliderVal"></span></div>
                </form>
            </div>

            <div>

                <form>
                    <h2>Тип рисунка</h2>
                    <div class="form-check">
                        <label>
                            <input type="radio" name="figureName" value="LineFigure" checked> <span class="label-text">Линия</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <label>
                            <input type="radio" name="figureName" value="RectangleFigure"> <span class="label-text">Прямоугольник</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <label>
                            <input type="radio" name="figureName" value="PenFigure"> <span
                                    class="label-text">Карандаш</span>
                        </label>
                    </div>
                    <div class="form-check">
                        <label>
                            <input type="radio" name="figureName" value="CircleFigure"> <span
                                    class="label-text">Круг</span>
                        </label>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-8">
            <? if (!empty($arResult["ERROR"])) { ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo join($arResult["ERROR"], ", ") ?>
                </div>
            <? } ?>
            <div id="canvas-block"
                 style=" border: 2px solid red; width: 404px; height: 404px;position: relative; padding:2px"></div>
            <form action="<?php echo $APPLICATION->GetCurPageParam() ?>" method="post" enctype="multipart/form-data">
                <input type='hidden' name="CANVAS[ACTION]" value="save"/>
                <?php echo bitrix_sessid_post() ?>
                <input type='hidden' name="CANVAS[ID]" value="<?php echo $arResult["ID"] ?>"/>
                <div id="canvas-form">
                    <?php foreach ($arResult["CANVAS_ITEMS"] as $arItem):
                        ?>
                        <input type='hidden' name="CANVAS[UF_DATA][]" value="<?php echo htmlspecialcharsbx($arItem,ENT_QUOTES) ?>"/>
                        <?
                    endforeach; ?>
                </div>
                <button name="CANVAS[SUBMIT]" value="Y">Сохранить</button>
            </form>

        </div>

    </div>

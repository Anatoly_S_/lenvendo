"use strict";


let canvas, currentParams;
$(function() {
console.log(150);

//инициализация слайдеров, события на смену значений слайдеров
    if(typeof $("#ex5").slider==='function') {
        $("#ex5").slider();
        $("#ex5").on("slide slideStop", function (slideEvt) {
            $("#ex5SliderVal").css('height', slideEvt.value + 'px');
            if (typeof canvas !== "undefined") {
                currentParams.setWight(slideEvt.value);
            }
        });


        $("#ex6").slider();
        $("#ex6").on("slide slideStop", function (slideEvt) {
            $("#ex6SliderVal").css('background', 'rgb(' + slideEvt.value + ',' + slideEvt.value + ',' + slideEvt.value + ')');

            currentParams.setColor(slideEvt.value);


        });
    }

//параметры обасти инструментов
    currentParams = new CurrentParams();

    //область рисования
    canvas = new Canvas($('#canvas-block'), $('#canvas-form'));
    //импорт
    canvas.import();
    //показ
    canvas.show();
    //подготовка к рисованию
    canvas.addLayer();
    currentParams.checkCurrentValues();
    canvas.start();

    //событие на смену типу рисунка
    $('input[name=figureName]').on('change', function () {
        currentParams.setFigureName($(this).val());
    });


});
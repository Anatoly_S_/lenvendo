<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="alert alert-success" role="alert">
    <a href="<?= str_replace('#ID#', $arResult["ID"], $arParams["EDIT_URL"]) ?>"> ссылка на рисунок </a><br/>
    Пароль для редактирования: <?= $arResult["UF_PASSWORD"] ?><br/>
    <a href="<?=$arParams["LIST_URL"] ?>"> к списку рисунков</a><br/>
    <a href="<?= str_replace('#ID#', 0, $arParams["EDIT_URL"]) ?>"> нарисовать еще</a><br/>
</div>

<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="container">
    <div class="row">
        <? if (!empty($arResult["ERROR"])) { ?>
            <div class="alert alert-danger" role="alert">
                <?php echo join($arResult["ERROR"], ", ") ?>
            </div>
        <? } ?>
        <div class="col-md-8">
            <form action="<?php echo $APPLICATION->GetCurPageParam() ?>" method="post" enctype="multipart/form-data">
                <?php echo bitrix_sessid_post() ?>
                <input type='hidden' name="CANVAS[ACTION]" value="password"/>
                <input type='hidden' name="CANVAS[ID]" value="<?php echo $arResult["ID"] ?>"/>
                <div class="col-md-4">
                    <div>
                        <h2>Пароль</h2>

                        <input type="text"  name="CANVAS[UF_PASSWORD]"/>

                    </div>
                    <div>
                        <button name="CANVAS[SUBMIT]" value="Y">отправить</button>
                    </div>
            </form>

        </div>

    </div>
</div>
<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");
$APPLICATION->SetPageProperty("title","Страница не существует");

$APPLICATION->IncludeComponent("realweb:redirect","",Array());

?>
    <style>
        #primary-page-area{
            width: 100%!important;
        }
        #primary-sidebar,
        .breadcrumbs,
        #pagetitle,
        .primary-interface-outer {
            display: none!important;
        }
    </style>
<div class="nowhere">
	<img src="/local/templates/materik-m/images/elements/nowhere.png" class="nowhere-face" alt="404">
	<div class="nowhere-title">
		Страница не существует
	</div>
	<a href="/" class="nowhere-away">Перейти на главную</a>
</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>